package com.canda.mulestac.mule4.notification;

import org.mule.runtime.api.notification.PipelineMessageNotification;
import org.mule.runtime.api.notification.PipelineMessageNotificationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * https://docs.mulesoft.com/mule-runtime/4.1/mule-server-notifications
 */
public class MulestacPipelineMessageNotificationListener implements PipelineMessageNotificationListener<PipelineMessageNotification>, InitializingBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(MulestacPipelineMessageNotificationListener.class);

	/**
	 *
	 */
	public MulestacPipelineMessageNotificationListener() {
		super();
		LOGGER.info("Created {}", this);
	}

	/**
	 *
	 */
	@Override
	public void onNotification(PipelineMessageNotification aNotification) {
		LOGGER.info("onNotification Notification={}", aNotification);
	}

	/**
	 *
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		LOGGER.info("afterPropertiesSet");
	}

}
